<?php include "admin_header.php"; ?>

<h1 class="page-header">Users</h1>

<!-- main content -->

<div class="box-content">

	<?php $create_user_url = base_url()."user_create.php"; ?>
	<p>
		<a href="<?= $create_user_url ?>">
			<button type="button" class="btn btn-primary"> &nbsp; &nbsp; Add User &nbsp; &nbsp;</button>
		</a>
	</p>


	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white list"></i><span class="break"></span>Record</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
					  	  <th>ID</th>
						  <th> First name</th>
						  <th> Middle Initial</th>
						  <th> Last Name</th>
						  <th> Course</th>
						  <th> Year</th>
						  <th> Date Violated</th>
						  <th> Violation</th>
						  <th> Summary</th>
						  <th class="span4">Actions</th>
					  </tr>
				  </thead>   
				  <tbody>
				  <?php 
				  		$table_name = "users";

				  		//get all records from users table
						$user_data = get($table_name);

						//fetch result set and pass it to an array (associative)
				  		foreach ($user_data as $key => $row) {
						$fisrtname = strtoupper($row['firstname']);
						$mi = $row['mi'];
						$lastname = strtoupper($row['lastname']);
						$course = strtoupper($row['course']);
						$year = $row['year'];
						$datev = $row['datev'];
						$violation = $row['violation'];
						$summary = $row['summary'];
				
						$id = $row['id'];

				  		$edit_user_url = base_url().'user_edit.php?id='.$id;
				  		$comply_user_url = base_url().'comply.php?id='.$id;
				  ?>
					<tr>
						<td class="center"><?= $id ?></td>
						<td class="center"><?= $fisrtname ?></td>
						<td class="center"><?= $mi ?></td>
						<td class="center"><?= $lastname ?></td>
						<td class="center"><?= $course ?></td>
						<td class="center"><?= $year ?></td>
						<td class="center"><?= $datev ?></td>
						<td class="center"><?= $violation ?></td>
						<td class="center"><?= $summary ?></td>

						<td class="center">
							<a class="btn btn-success" href="<?= $comply_user_url ?>">
								<i class="halflings-icon white check"></i> comply
							</a>
							<a class="btn btn-info" href="<?= $edit_user_url ?>">
								<i class="halflings-icon white edit"></i> edit
							</a>
						</td>
					</tr>
					<?php } ?>
				  </tbody>
				</table> 
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>