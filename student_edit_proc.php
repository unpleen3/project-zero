<?php
	
	include "perfect_function.php";

	$table_name = 'tbl_students';

	//get user ID from URL
	$id = $_POST['id'];
	$studentid = $_POST['studentid'];
	$firstname = $_POST['firstname'];
	$mname = $_POST['mi'];
	$lastname = $_POST['lastname'];
	$course = $_POST['course'];
	$year = $_POST['year'];
	$section = $_POST['section'];
	$email = $_POST['email'];
	$contact = $_POST['contact'];
	$gender = $_POST['gender'];
	$schoolyear = $_POST['schoolyear'];
	$sem = $_POST['sem'];


	$student_editedvalues = array (
			"student_id" => $studentid, 
			"firstname" => $firstname,
			"middlename" => $mname,  
			"lastname" => $lastname, 
			"course" => $course, 
			"year" => $year, 
			"section" => $section, 
			"email" => $email, 
			"contact" => $contact, 
			"gender" => $gender, 
			"school_year" => $schoolyear, 
			"sem" => $sem
			
	);

	update($student_editedvalues, $id, $table_name);

	$recent_id = get_max($table_name);
$whomai = _get_username_from_id($_SESSION['user_id']);

$text = "User $whomai has successfully edited a student";
$text.= " with an ID of $recent_id";
save_logs($text); 

	header("Location: student_manage.php");
?>