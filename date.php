<?php

	include 'perfect_date.php';

	echo time();
	echo "<hr> <br>";
	echo get_nice_date(time(), "full");
	echo "<hr> <br>";
	echo get_nice_date(time(), "cool");
	echo "<hr> <br>";
	echo get_nice_date(time(), "shorter");
	echo "<hr> <br>";
	echo get_nice_date(time(), "mini");
	echo "<hr> <br>";
	echo get_nice_date(time(), "oldschool");
	echo "<hr> <br>";
	echo get_nice_date(time(), "datepicker");
	echo "<hr> <br>";
	echo get_nice_date(time(), "datepicker_us");
	echo "<hr> <br>";
	echo get_nice_date(time(), "monyear");
?>