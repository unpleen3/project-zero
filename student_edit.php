<?php include "admin_header.php"; ?>

<h1 class="page-header">Edit Student</h1>

<!-- main content -->
<?php
	//get user ID from URL
	$id = $_GET['id'];

	$form_location = base_url()."student_edit_proc.php?id=".$id; 
	$table_name = "tbl_students";

	//select user record where ID (column from table) = user ID from URL 
	$get_userData = get_where($table_name, $id);

	//fetch result and pass it  to an array
	foreach ($get_userData as $key => $row) {
		$id = $row['id'];
		$studentid = $row['student_id'];
		$firstname = $row['firstname'];
		$mname = $row['middlename'];
		$lastname = $row['lastname'];
		$course = $row['course'];
		$year = $row['year'];
		$section = $row['section'];
		$email = $row['email'];
		$contact = $row['contact'];
		$gender = $row['gender'];
		$schoolyear = $row['school_year'];
		$sem = $row['sem'];
	}
	?>
<div class="box-content">

	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white file"></i><span class="break"></span>Student</h2>
			</div>
			<div class="box-content">

				<form class="form-horizontal" method="post" action="<?= $form_location ?>">
					<input type="hidden" class="span4" name="id" value="<?= $id ?>" required>
					<fieldset>
						<div class="control-group">
							<label class="control-label">Student ID:</label>
							<div class="controls">
								<input type="text" class="span4" name="studentid" value="<?= $studentid ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Firstname:</label>
							<div class="controls">
								<input type="text" class="span4" name="firstname" value="<?= $firstname ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Middle Name:</label>
							<div class="controls">
								<input type="text" class="span4" name="mi" value="<?= $mname ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Last Name:</label>
							<div class="controls">
								<input type="text" class="span4" name="lastname" value="<?= $lastname ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Course:</label>
							<div class="controls">
								<select class="span4" name="course" required>
									<?php
									echo "
									<option value='".$course."'>".$course."</option>
									<option value='BLIS'>BLIS</option>
									<option value='BSIT'>BSIT</option>
									<option value='BSCE'>BSCE</option>
									<option value='BSCOE'>BSCOE</option>
									<option value='BSENSE'>BSENSE</option>
									<option value='BSGE'>BSGE</option>
									";
									?>

								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Year:</label>
							<div class="controls">
								<select class="span4" name="year" required>
									<?php
									echo "
									<option value='".$year."'>".$year."</option>
									<option value='1'>1</option>
									<option value='2'>2</option>
									<option value='3'>3</option>
									<option value='4'>4</option>
									<option value='5'>5</option>
									";
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Section:</label>
							<div class="controls">
								<select class="span4" name="section" required>
									<?php
									echo "
									<option value='".$section."'>".$section."</option>
									<option value='A'>A</option>
									<option value='B'>B</option>
									<option value='C'>C</option>
									<option value='D'>D</option>
									<option value='E'>E</option>
									";
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Email:</label>
							<div class="controls">
								<input type="text" class="span4" name="email" value="<?= $email ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Contact:</label>
							<div class="controls">
								<input type="text" class="span4" name="contact" value="<?= $contact ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Gender:</label>
							<div class="controls">
								<select class="span4" name="gender" required>
									<?php
									echo "
									<option value='".$gender."'>".$gender."</option>
									<option value='F'>F</option>
									<option value='M'>M</option>
									";
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">School Year:</label>
							<div class="controls">
								<select class="span4" name="schoolyear" required>
									<?php
									echo "
									<option value='".$schoolyear."'>".$schoolyear."</option>
									<option value='2019-2020'>2019-2020</option>
									<option value='2020-2021'>2020-2021</option>
									<option value='2021-2022'>2021-2022</option>
									<option value='2022-2023'>2022-2023</option>
									<option value='2023-2024'>2023-2024</option>
									";
									?>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Semester:</label>
							<div class="controls">
								<select class="span4" name="sem" required>
									<?php
									echo "
									<option value='".$sem."'>".$sem."</option>
									<option value='1st'>1st</option>
									<option value='2nd'>2nd</option>
									<option value='Summer'>Summer</option>
									";
									?>
								</select>
							</div>
						</div>

						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
							<a class="btn" href="user_manage.php">Cancel</a>
						</div>
						
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>