<?php include('admin_header.php');?>

<h1 class="page-header">Complied List</h1>

<div class="box-content">

<p>
<a href="#">
<button type="button" class="btn btn-primary"> &nbsp; &nbsp; Print Complied Violation History &nbsp; &nbsp;</button>
</a>
</p>

<div class="row-fluid sortable">
<div class="box span12">

<div class="box-header">
<h2><i class="halflings-icon white list"></i><span class="break"></span>Complied List Summary</h2>
</div>

<div class="box-content">

<table class="table bootstrap-datatable datatable">
    <thead>
    <tr>
    <th>Student ID</th>
    <th>Student Name</th>
    <th>Course</th>
    <th>Year</th>
    <th>Section</th>
    <th>Violation(s)</th>
    <th>Date of Violation</th>
    <th>Compliance by the Student</th>
    <th>Date of Completion</th>
    <th>Action Taken</th>
    
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td>Test ID</td>
    <td>Test Name</td>
    <td>Test Course</td>
    <td>Test Year</td>
    <td>Test Section</td>
    <td>Test Violation</td>
    <td>Test Date of Violation</td>
    <td>Test Compliance by the Student</td>
    <td>Test Date of Completion</td>
    <td>Test Action Taken</td>
    </tr>
    </tbody>


    </table>
    </div>
    </div>
    </div>
    </div>
<?php include('admin_footer.php');?>