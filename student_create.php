<?php include "admin_header.php"; ?>

<h1 class="page-header">Add Student</h1>

<!-- main content -->

<div class="box-content">

	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white file"></i><span class="break"></span>STUDENT</h2>
			</div>

			<div class="box-content">
				<?php $form_location = base_url()."student_create_proc.php"; ?>
				<form class="form-horizontal" method="post" action="<?= $form_location ?>">
					<fieldset>
						<div class="control-group">
							<label class="control-label">Student ID:</label>
							<div class="controls">
								<input type="text" class="span4" name="studentid" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Firstname:</label>
							<div class="controls">
								<input type="text" class="span4" style="text-transform: uppercase;" name="firstname" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Middle Name:</label>
							<div class="controls">
								<input type="text" class="span4" name="mi" style="text-transform: uppercase;" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Last Name:</label>
							<div class="controls">
								<input type="text" class="span4" name="lastname" style="text-transform: uppercase;" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Course:</label>
							<div class="controls">
								<select class="span4" name="course" required>
									<option value="">Select...</option>
									<option value="BLIS">BLIS</option>
									<option value="BSIT">BSIT</option>
									<option value="BSCE">BSCE</option>
									<option value="BSCOE">BSCOE</option>
									<option value="BSENSE">BSENSE</option>
									<option value="BSGE">BSGE</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Year:</label>
							<div class="controls">
								<select class="span4" name="year" required>
									<option value="">Select...</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Section:</label>
							<div class="controls">
								<select class="span4" name="section" required>
									<option value="">Select...</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Email:</label>
							<div class="controls">
								<input type="text" class="span4" name="email" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Contact:</label>
							<div class="controls">
								<input type="text" class="span4" name="contact" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Gender:</label>
							<div class="controls">
								<select class="span4" name="gender" required>
									<option value="">Select...</option>
									<option value="F">F</option>
									<option value="M">M</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">School Year:</label>
							<div class="controls">
								<select class="span4" name="schoolyear" required>
									<option value="">Select...</option>
									<option value="2019-2020">2019-2020</option>
									<option value="2020-2021">2020-2021</option>
									<option value="2021-2022">2021-2022</option>
									<option value="2022-2023">2022-2023</option>
									<option value="2023-2024">2023-2024</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Semester:</label>
							<div class="controls">
								<select class="span4" name="sem" required>
									<option value="">Select...</option>
									<option value="1st">1st</option>
									<option value="2nd">2nd</option>
									<option value="Summer">Summer</option>
								</select>
							</div>
						</div>

						

						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
							<a class="btn" href="user_manage.php">Cancel</a>
						</div>
						
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>