-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2018 at 04:16 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_zero`
--

-- --------------------------------------------------------

--
-- Table structure for table `bible_verses`
--

CREATE TABLE `bible_verses` (
  `id` int(11) NOT NULL,
  `book` varchar(65) NOT NULL,
  `chapter` varchar(5) NOT NULL,
  `verse` varchar(5) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bible_verses`
--

INSERT INTO `bible_verses` (`id`, `book`, `chapter`, `verse`, `text`) VALUES
(4, 'Proverbs', '18', '13', 'We have two ears and one mouth for a reason. It\'s always best to listen first in any situation and speak last, after you\'ve had time to consider fully what has been said'),
(5, 'Philippians', '3', '13-14', 'Brothers and sisters, I do not consider myself yet to have taken hold of it. But one thing I do: Forgetting what is behind and straining toward what is ahead, I press on toward the goal to win the prize for which God has called me heavenward in Christ Jesus.'),
(6, '1 Timothy', '62a', '12', 'Fight the good fight of the faith. Take hold of the eternal life to which you were called when you made your good confession in the presence of many witnesses.'),
(8, 'Jeremiah', '29', '11', 'For I know the plans I have for you, plans to prosper you and not to harm you, plans to give you hope and a future.'),
(9, 'Giled', '1', '2', '3'),
(10, 'adw', '12', '34', '412asdasd'),
(13, '123', '345', '54', '7656');

-- --------------------------------------------------------

--
-- Table structure for table `latest_news`
--

CREATE TABLE `latest_news` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `page_keywords` text NOT NULL,
  `page_description` text NOT NULL,
  `page_content` text NOT NULL,
  `date_published` int(11) NOT NULL,
  `posted_by` int(11) NOT NULL,
  `page_picture` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `latest_news`
--

INSERT INTO `latest_news` (`id`, `page_title`, `page_url`, `page_keywords`, `page_description`, `page_content`, `date_published`, `posted_by`, `page_picture`) VALUES
(1, 'Cyber Summit 2019', 'Cyber-Summit-2019', 'spup site cybersummit 2019', '20th Annual ITE Regional convention', '<h1><font face=\"Comic Sans MS\" style=\"\" color=\"#009900\"><b>ST. PAUL UNIVERSITY PHILIPPINES</b></font></h1><div style=\"\"><font color=\"#ff0000\" style=\"\" size=\"4\" face=\"Courier New\"><b style=\"\">SCHOOL OF INFORMATION TECHNOLOGY &amp; ENGINEERING (SITE)</b></font></div><div style=\"font-size: 10pt;\"><font color=\"#ff0000\" face=\"Comic Sans MS\"><b><br></b></font></div><div style=\"\"><font color=\"#000066\" face=\"Impact\" style=\"\" size=\"6\"><b style=\"\">CYBER SUMMIT 2018</b></font></div>', 1524188596, 1, 'John Cena.png'),
(5, 'Roque appeals to Boracay task force to ease media restrictions', 'Roque-appeals-to-Boracay-task-force-to-ease-media-restrictions', 'roque boracay', '\'A comparison was made between Boracay and Marawi. I said, wala naman IEDs sa Boracay,\' says Pesidential Spokesperson Harry Roque ', '<div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">MANILA, Philippines â€“ Presidential Spokesperson Harry Roque himself asked officials of the Boracay task force if media covering the island\'s closure can be given more freedom during coverage.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Roque said he made the appeal during the inter-agency task force\'s meeting on Friday, April 20, in MalacaÃ±ang.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">\"I personally appealed to the inter-agency [task force] to reconsider the 9 to 5 rule applied to members of the media to cover,\" he said during a Palace press conference.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">The task force had earlier announced that media workers would only be allowed inside Boracay from 9 am to 5 pm during the 6-month closure.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Reporters and their crew would be required to spend their nights outside the island and would have to be accompanied by escorts while in Boracay.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">A reporter, during his recent press conference in Boracay, expressed opposition to the media restrictions as this would affect coverage of Boracay\'s closure.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">\"I do agree that media does not sleep and that, of course, a comparison was made between Boracay and Marawi. I said, wala naman IEDs sa (there are no improvised explosive devices in) Boracay,\" said Roque.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">One of the suggestions discussed during the meeting was the provision of media cards that would allow journalists to be \"treated as residents of the island,\" said Roque.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">However, Duterte\'s spokesman said he was not sure if the task force would act on his appeal.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">\"I think I persuaded the Cabinet,\" he said.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Based on the guidelines, the accreditation of television networks will be limited to a maximum of 12 people, while the accreditation for radio, print, wire agencies, and online platforms will be limited to a maximum of 5 people.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Issues have been raised about the constitutionality of the media restrictions, with legal experts saying it can be considered prior restraint.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Media groups that want to challenge the media guidelines would have a solid case, they said.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Roque also gave assurances that the proclamation of a Boracay state of calamity and the executive order on the island\'s closure will be released soon by MalacaÃ±ang.</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Boracay\'s closure begins on Thursday, April 26.&nbsp;</span></font></div>', 1524533364, 1, 'roque.jpg'),
(6, 'Boracay: From pristine island to fragile paradise', 'Boracay:-From-pristine-island-to-fragile-paradise', 'Boracay Closed', 'Despite counting itself among the best islands of the world, Boracay has struggled to keep up with its fame. A look at its history shows warning signs at least a decade-old.', '<p id=\"ext-gen11694\" class=\"p1\" style=\"box-sizing: border-box; margin: 8px 0px 24px; font-size: 16px; line-height: 1.5; color: rgb(76, 76, 76); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">MANILA, Philippines â€“ Itâ€™s been consistently named one of the best islands in the world, but a glimpse at Boracayâ€™s history shows how the island has struggled to keep up with its fame.</p><p class=\"p1\" style=\"box-sizing: border-box; margin: 8px 0px 24px; font-size: 16px; line-height: 1.5; color: rgb(76, 76, 76); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\"><span class=\"s1\" style=\"box-sizing: border-box;\">After about two months since President Rodrigo Duterte&nbsp;<a href=\"https://www.rappler.com/nation/195703-duterte-warning-close-boracay\" target=\"_blank\" style=\"box-sizing: border-box; background: transparent; color: rgb(231, 98, 41); text-decoration-line: none; transition: color 0.2s ease;\">first made mention</a>&nbsp;of Boracay in February 2018, the tourist island on Thursday, April 26, closed its shores for 6 months.&nbsp;<span class=\"Apple-converted-space\" style=\"box-sizing: border-box;\">(<a href=\"https://www.rappler.com/newsbreak/in-depth/200075-duterte-decision-boracay-closure\" target=\"_blank\" style=\"box-sizing: border-box; background: transparent; color: rgb(231, 98, 41); text-decoration-line: none; transition: color 0.2s ease;\">INSIDE STORY: How Duterte decided on Boracay closure</a>)</span></span></p><p class=\"p1\" style=\"box-sizing: border-box; margin: 8px 0px 24px; font-size: 16px; line-height: 1.5; color: rgb(76, 76, 76); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\"><span class=\"s1\" style=\"box-sizing: border-box;\">\"You go into the water, itâ€™s smelly. Smell of what? Shit. Because everything that goes out in Boracay...itâ€™s destroying the environment or the Republic of the Philippines and creating a disaster coming,\" Duterte said last February 10.</span></p><p class=\"p1\" style=\"box-sizing: border-box; margin: 8px 0px 24px; font-size: 16px; line-height: 1.5; color: rgb(76, 76, 76); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\"><span class=\"s1\" style=\"box-sizing: border-box;\"><span class=\"Apple-converted-space\" style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box;\">During the closure period, various government agencies and stakeholders will seek to address the islandâ€™s&nbsp;<a href=\"https://www.rappler.com/nation/201085-green-algae-shore-boracay\" target=\"_blank\" style=\"box-sizing: border-box; background: transparent; color: rgb(231, 98, 41); text-decoration-line: none; transition: color 0.2s ease;\">environmental woes</a>.<span class=\"Apple-converted-space\" style=\"box-sizing: border-box;\">&nbsp;</span></span></span></span><span class=\"s1\" style=\"box-sizing: border-box;\">How did Boracay go from a pristine island to a fragile paradise in just a few years?<span class=\"Apple-converted-space\" style=\"box-sizing: border-box;\">&nbsp;(<a href=\"https://www.rappler.com/nation/201089-boracay-closure-last-few-minutes-live-video\" target=\"_blank\" style=\"box-sizing: border-box; background: transparent; color: rgb(231, 98, 41); text-decoration-line: none; transition: color 0.2s ease;\">WATCH: The last few minutes before Boracay is closed</a>)</span></span></p>', 1525081941, 7, 'boracay.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `text`, `datetime`) VALUES
(1, 'User fmasigan has successfully logged in.', 1524619523),
(2, 'User cpal has successfully logged in.', 1524619881),
(3, 'User rcalizar has successfully logged in.', 1524619891),
(4, 'User zgavino has successfully logged in.', 1524619898),
(5, 'User cbabaran has successfully logged in.', 1524619917),
(6, 'User cbabaran has successfully logged in.', 1525080446),
(7, 'User cbabaran has successfully logged in.', 1525080615),
(8, 'User rcalizar has successfully logged in.', 1525081742),
(9, 'User cbabaran has successfully logged in.', 1525082620),
(10, 'User cbabaran has successfully logged in.', 1525220728);

-- --------------------------------------------------------

--
-- Table structure for table `sliderpics`
--

CREATE TABLE `sliderpics` (
  `id` int(11) NOT NULL,
  `picture` varchar(65) NOT NULL,
  `alt` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliderpics`
--

INSERT INTO `sliderpics` (`id`, `picture`, `alt`) VALUES
(8, 'IMG2.jpg', 'IMG2.jpg'),
(9, 'IMG3.jpg', 'IMG3.jpg'),
(12, '16325988_858483727627281_336703008_o.png', '16325988_858483727627281_336703008_o.png'),
(14, 'the_beatles.jpg', 'the_beatles.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `password` varchar(125) NOT NULL,
  `firstname` varchar(65) NOT NULL,
  `lastname` varchar(65) NOT NULL,
  `acct_type` int(2) NOT NULL,
  `photo` varchar(65) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `acct_type`, `photo`, `status`) VALUES
(1, 'cbabaran', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Carlos', 'Babaran', 1, 'ed_sheeran.jpg', 1),
(2, 'rpugeda', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Rucelja', 'Pugedaa', 2, 'oasis.jpg', 1),
(5, 'sbeligan', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Shaina', 'Beligan', 2, '', 1),
(7, 'rcalizar', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Rheiner', 'Calizar', 2, '', 1),
(8, 'rcanlas', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Regina', 'Canlas', 2, '', 1),
(10, 'kcuevas', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Kharen', 'Cuevas', 2, '', 1),
(12, 'dgdumaua', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Diego Gracias', 'Dumaua', 2, '', 1),
(13, 'kfeliciano', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'King', 'Feliciano', 2, '', 1),
(16, 'zgavino', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Zahn Xander', 'Gavino', 2, '', 1),
(17, 'jginez', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Joshua', 'Ginez', 2, '', 1),
(18, 'jhernandez', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Jyzel', 'Hernandez', 2, '', 1),
(19, 'cpal', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Chrysiele', 'Pal', 2, '', 1),
(20, 'msabban', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Monique', 'Sabban', 2, '', 1),
(21, 'jtangan', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Johana Mae', 'Tangan', 2, '', 1),
(23, 'duanang', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Donald', 'Uanang', 2, '', 1),
(24, 'sgumarang', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Sheena', 'Gumarang', 1, '', 1),
(25, 'vcgumabay', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Ma. Visitacion', 'Gumabay', 1, '', 1),
(26, 'fmasigan', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Fe', 'Masigan', 1, '', 1),
(28, 'johncena', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Carlos', 'Babaran', 2, '', 0),
(29, 'mcacacho', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Mars', 'Cacacho', 2, '', 0),
(30, 'Giled', '', '$2y$11$rwVzP.728Op/hP6oryf/HeMQLKTAh7gKsR8I2vjvoOuW5Itr/RQei', 'Jan Giled', 'Reyes', 1, '', 1),
(31, 'faith', '', '$2y$11$VlfhtHalspY35tkv80/15O0guRJSTqSEy0WiuzTzRRmv2vL2e5QYe', 'Faith', 'Collado', 1, '', 1),
(32, '', '', '$2y$11$ZUsarRL8T3EuVtIkqU3v4OYZGw5cAZPjVrBmDPd0mWLqfKMjskXre', '', '', 0, '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bible_verses`
--
ALTER TABLE `bible_verses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `latest_news`
--
ALTER TABLE `latest_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliderpics`
--
ALTER TABLE `sliderpics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bible_verses`
--
ALTER TABLE `bible_verses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `latest_news`
--
ALTER TABLE `latest_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliderpics`
--
ALTER TABLE `sliderpics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
