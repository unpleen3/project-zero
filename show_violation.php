<?php include "admin_header.php"; ?>

	
  <div class="content-wrapper">
    <div class="container-fluid">
	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
        	<i class="icon-table"></i> Student Violations 
    	</div>
        <div class="card-body">
				<?php
					$id = isset($_GET['id']) ? $_GET['id'] : '';
					$sql_student = get_where("tbl_students",$id);

					$row = mysqli_fetch_array($sql_student, MYSQLI_BOTH);
					$ss = $row['id'];
					$student_id = $row['student_id'];
					$fullname = $row['lastname'].", ".$row['firstname']." ".$row['middlename'];
					echo "<h1>".$student_id." - ".strtoupper($fullname)."</h1>";

				?>
				<hr/>
				<?php 
					if(isset($_POST['schoolyear']) && isset($_POST['sem']) ){
						$sy = $_POST["schoolyear"]; 
						$sem = $_POST["sem"]; 
					}
					else {
						$sy = $current_school_year;
						$sem = $current_sem;
					}
				?>
				<form  method="post" action="" role="form" enctype="multipart/form-data">
					<div class="row">
						<div class="col-lg-6">
						<?php 
							echo "<a href='add_violation1.php?id=".$id."' title='add violation' class='btn btn-danger'><i class='fa fa-plus'></i> add violation </a>&nbsp";  					
						?>
						</div>
					</div>
				</form>
					<hr/>
				
					<div class="table-responsive">
    <table class="table table-list-search-table-bordered table-condensed custab small">
    <thead>
    <tr>
    <th>Violation</th>
    <th>Date of Violation</th>
    <th>Complience by the Student</th>
    <th>Date of Compliance</th>
    <th>Action Taken</th>
	<th>Update</th>
    
    </tr>
    </thead>
    
    <tbody>
	
			<?php
				$result = violation_student($student_id);
				$S = 1;
				if ($result->num_rows > 0) {
			  // output data of each row
			    while($row = $result->fetch_assoc()) {
				$violation = $row['violation'];
				$violation_id = $row['id'];
				$date = date("F d, Y", strtotime($row['date']));
				$B1 = $row['b1'];
				$B2 = $row['b2'];
			
								?>		
	<form method="POST" action="violation_update.php?q=update&id=<?= $id ?>&v_id=<?= $violation_id ?>">

    <tr>
    <td>
	<?php
										  switch ($violation) {
											case 'A':
												echo "A. Haircut/punky hair </p>";
												break;
											case 'B':
												echo "B. Coloured Hair ";
												break;
											case 'C':
												echo "C. Unprescribed Undergarment ";
												break;
											case 'D':
												echo "D. Unprescribed Shoes ";
												break;
											case 'E':
												echo "E. Long/Short Skirt ";
												break;
											case 'F':
												echo "F. Being noisy along corridors";
												break;
											case 'G':
												echo "G. Not wearing of ID Properly";
												break;
											case 'H':
												echo "H. Earring/Tounge Ring";
												break;
											case 'I':
												echo "I. Wearing of Cap inside the Campus";
												break;
												}

												?>
	
	</td>

	 <td><?php echo  $date; ?></td>

    <td>
	<div class="form-group">
	<input class="form-control form-control-lg" type="text" placeholder="Enter Compliance">
  	</div>
    </td>

    <td>  
    <div class="form-group">
    <input type="text" name="doc" class="form-control" id="exampleFormControlInput1" value="<?= date("Y/m/d") ?>" id="picker" disabled>
    </div>
    </td>

    <td>
    <div class="form-group">
    <select name="action" class="form-control" id="exampleFormControlSelect1">
      <option>Complied</option>
      <option>Expelled</option>
      <option>Suspended</option>
    </select>
     </div>
    </td>
	<td><input type="submit" value="Comply" class="btn btn-warning"></td>
    </tr>

	<?php 
			}
		}
			?>
				</form>


    
    </tbody>


    </table>
    </div>

        	</div>
        <div class="card-footer small text-muted">
			<?php 
				
				// $result1 = violation_student_count("tbl_violation","B1",0);
				// $c1 = $result1['c'];
	
				// $result2 = violation_student_count("tbl_violation","B2",0);
				// $c2 = $result1['c'];
				
					// if ($c1 <= 0  && $c2 <= 0 && $S != 0) {
					// 	echo "<span><a  href='print_clearance?id=".$id."' class='btn btn-success btn-block'><i class='fa fa-print'></i> print clearance </a></span>";
					// }
					// else if ($S == 0) {
					// 	echo "<span><a  href='print_clearance?id=".$id."' class='btn btn-success btn-block'><i class='fa fa-print'></i> print clearance </a></span>";
					// }
					// else {
					// 	echo "<div class='alert alert-danger text-center'>Please comply first before printing clearance!</div>";
					// }

			?>

		</div>
      </div>
    </div>
</div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php include "admin_footer.php"; ?>


