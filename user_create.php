<?php include "admin_header.php"; ?>

<h1 class="page-header">Add User</h1>

<!-- main content -->

<div class="box-content">

	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white file"></i><span class="break"></span>Users</h2>
			</div>

			<div class="box-content">
				<?php $form_location = base_url()."user_create_proc.php"; ?>
				<form class="form-horizontal" method="post" action="<?= $form_location ?>">
					<fieldset>
						<div class="control-group">
							<label class="control-label">ID:</label>
							<div class="controls">
								<input type="text" class="span4" name="id" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Firstname:</label>
							<div class="controls">
								<input type="text" style="text-transform: uppercase;" class="span4" name="firstname" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Middle Initial:</label>
							<div class="controls">
								<input type="text" style="text-transform: uppercase;" class="span4" name="mi" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Last Name:</label>
							<div class="controls">
								<input type="text" style="text-transform: uppercase;" class="span4" name="lastname" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Course:</label>
							<div class="controls">
								<input type="text" style="text-transform: uppercase;" class="span4" name="course" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Year:</label>
							<div class="controls">
								<select class="span4" name="year" required>
									<option value="">Select...</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Date Violated:</label>
							<div class="controls">
								<input type="date" class="span4" name="datev" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Violation:</label>
							<div class="controls">
								<select class="span4" name="violation" required>
									<option value="">Select...</option>
									<option value="sample violation 1">sample violation 1</option>
									<option value="sample violation 2">sample violation 2</option>
									<option value="sample violation 3">sample violation 3</option>
									<option value="sample violation 4">sample violation 4</option>
								</select>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Summary of Violation:</label>
							<div class="controls">
								<textarea name="summary" style="resize:none;" id="textarea2" rows="4"></textarea>
							 </div>
						</div>

						

						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="submit" value="Submit">Save changes</button>
							<a class="btn" href="user_manage.php">Cancel</a>
						</div>
						
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>