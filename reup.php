<?php include "admin_header.php"; ?>

<body>
    <div class="content-wrapper">
    <div class="container-fluid">
    <div class="table-responsive">
    <table class="table table-list-search-table-bordered table-condensed custab small">
    <thead>
    <tr>
    <th>Violation</th>
    <th>Date of Violation</th>
    <th>Complience by the Student</th>
    <th>Date of Compliance</th>
    <th>Action Taken</th>
    
    </tr>
    </thead>
    
    <tbody>

    <tr>

    <td>TEST (Fetch Data of Violations)</td>

    <td>TEST (Fetch Data of Dates of Violation)</td>

    <td>
  <div class="form-group">
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Complied</option>
      <option>Not Complied</option>
    </select>
  </div>
    </td>

    <td>  
    <div class="form-group">
    <input type="date" class="form-control" id="exampleFormControlInput1" id="picker">
    </div>
    </td>

    <td>
    <div class="form-group">
    <select class="form-control" id="exampleFormControlSelect1">
      <option>Suspended</option>
      <option>Expelled</option>
      <option>Complied</option>
    </select>
     </div>
    </td>

    </tr>


    
    
    </tbody>


    </table>
    </div>
    </div>
    </div>
    
<?php include('admin_footer.php');?>
<!--Script of Date Picker-->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>   
</body>
</html>